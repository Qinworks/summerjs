//global basic properties
var _ = _ || {i: 0},//object container
    _objId = 1,     //object id counter
    w = window,     //document window
    Loop = function () {
    },
    rAF = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
//expose objects to global scope
w.S = w.s = w.Summer = _[(_.i)++] =
    //summer bootstrap function
    function (selector, context) {
        return s.modules.selector.call(null, selector, context);
    };
//the lib version
s.version = "0.1.0";
//the source level
s.level = 1;
//all defined modules goes here
s.modules = {
    /* init fallback */
    selector: Loop,
    scope: {
        _: {}, //singleton
        __: {} //alias
    },
    /* gen module id */
    genId: function () {
        return _objId++;
    }
};
//framework properties
s.props = {ready: []};
//on ready callback chain
s.onReady = function (callback) {
    if (isFunction(callback)) {
        s.props.ready.push(callback);
    }
};
/**
 * switch and return different version of Summer
 * @param level Summer source level
 * @returns {*}
 */
var use = window.use = use || function (level) {
    var i = _.i, o;
    while (i--) {
        if (_[i].level === level) {
            o = _[i];
            break;
        }
    }
    window.S = window.s = window.Summer = o;
    return o;
};

/**
 *
 * @type {Function}
 */
var toStr = Object.prototype.toString;
/**
 *
 * @param o
 * @returns {String}
 */
function type(o) {
    return toStr.call(o);
}
/**
 *
 * @param o
 * @param a
 * @returns {Boolean}
 */
function typeOf(o, a) {
    return typeof o === a;
}

/**
 *
 * @param o
 * @param a
 * @returns {boolean}
 */
function isType(o, a) {
    return type(o) === a;
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */

function isString(obj) {
    return typeOf(obj, 'string');
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */

function isEmptyString(obj) {
    return !noGapInString(obj);
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */

function noGapInString(obj) {
    return isString(obj) && !/\s+/g.test(obj);
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */

function isObject(obj) {
    return typeOf(obj, 'object');
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */
function isUndefined(obj) {
    return typeOf(obj, 'undefined');
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */
function isFunction(obj) {
    return isUndefined(obj) ? false : typeOf(obj, 'function');
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */
function isBoolean(obj) {
    return isUndefined(obj) ? false : typeOf(obj, 'boolean');
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */
function isNumber(obj) {
    return typeof obj === "number";
}

/**
 *
 * @param ele
 * @returns {Boolean}
 */
function isNodeList(ele) {
    return isUndefined(ele) ? false : isType(ele, "[object NodeList]");
}

/**
 *
 * @param o
 * @returns {Boolean}
 */
function isArray(o) {
    return isUndefined(o) ? false : isType(o, '[object Array]');
}

/**
 *
 * @param o
 * @returns {Boolean}
 */
function isArrayLike(o) {

    if (!o) return false;
    if (isArray(o)) return true;
    if (isNodeList(o)) return true;
    if (isNumber(o.length) && (o.length - 1 in o)) return true;

    return o.constructor && o.constructor.name === "HTMLCollection";
}

/**
 *
 * @param e
 * @returns {Boolean}
 */

function isDocument(e) {
    return e.nodeType && e.nodeType === 9 && e.nodeName && e.nodeName === "#document";
}

/**
 *
 * @param e
 * @returns {Boolean}
 */
function isBody(e) {
    return e.nodeType && e.nodeType === 1 && e.nodeName && e.nodeName.toLowerCase() === "body";
}

/**
 *
 * @param e
 * @returns {Boolean}
 */
function isWindow(e) {
    return !!(e && e.history && e.location && e.document);
}

/**
 *
 * @param e
 * @returns {Boolean}
 */
function isElementLike(e) {
    return !!(e && e.nodeType === 1);
}

/**
 *
 * @param e
 * @returns {Boolean}
 */
function isTextNode(e) {
    return !!(e && e.nodeType === 3);
}

/**
 *
 * @param e
 * @returns {Boolean}
 */
function isDocumentFragment(e) {
    return isType(e, '[object DocumentFragment]')
}

/**
 *
 * @param obj
 * @returns {Boolean}
 */
function isPlainObject(obj) {
    return !!obj && typeOf(obj, 'object') && obj.constructor === Object;
}

/**
 *
 * @param val
 * @returns {boolean}
 */
function isArguments(val) {
    return isType(val, '[object Arguments]');
}

/**
 *
 * @param source
 * @param target
 * @param replacement
 * @returns {XML|string|void}
 */
function replace(source, target, replacement) {
    return source.replace(
        new RegExp(target.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'),
        replacement);
}

/**
 *
 * @param str
 * @returns {String}
 */
function toCamel(str) {
    if (noGapInString(str)) {
        return str.replace(camelReg, function (a, b) {
            return b.toUpperCase();
        });
    }
    return str;
}

/**
 *
 * @param arr
 * @param callback
 */
function eachArray(arr, callback) {
    if (!isArray(arr))return;
    callback = callback || Loop;
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        if (callback(item, i, arr) === false) {
            break;
        }
    }
}

/**
 *
 * @param arr
 * @param v
 * @returns {number}
 */
function indexOfArray(arr, v) {
    var index = -1;
    eachArray(arr, function (value, i) {
        if (v === value) {
            index = i;
            return false;
        }
        return true;
    });
    return index;
}

/**
 *
 * @type {Function}
 */
objectKeys = Object.keys || (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        dontEnumsLength = dontEnums.length;

    return function (obj) {
        if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null) {
            throw new TypeError('Object.keys called on non-object');
        }

        var result = [];

        for (var prop in obj) {
            if (hasOwnProperty.call(obj, prop)) {
                result.push(prop);
            }
        }

        if (hasDontEnumBug) {
            for (var i = 0; i < dontEnumsLength; i++) {
                if (hasOwnProperty.call(obj, dontEnums[i])) {
                    result.push(dontEnums[i]);
                }
            }
        }
        return result;
    };
})();

/**
 *
 * @param obj
 * @param callback
 */
function eachOwnKeys(obj, callback) {
    var keys = objectKeys(obj),
        len = keys.length;
    while (len--) {
        var key = keys[len];
        callback(key, obj[key]);
    }
}

/**
 *
 * @param obj
 * @param callback
 */
function eachKeys(obj, callback) {
    if (obj) {
        for (var key in obj) {
            if (callback(key, obj[key], obj) === false) {
                break;
            }
        }
    }
}
/**
 *
 * @param obj
 * @param callback
 */
function eachKeysExcept(obj, except, callback) {
    if (obj && noGapInString(except)) {
        for (var key in obj) {
            if (except.indexOf(key) != -1) {
                continue;
            }
            if (callback(key, obj[key], obj) === false) {
                break;
            }
        }
    }
}

/**
 *
 * @param obj
 * @param callback
 * @returns {*}
 */
function each(obj, callback) {
    if (isArrayLike(obj)) {
        eachArray(obj, callback);
    } else {
        eachKeys(obj, callback);
    }
    return obj;
}

/**
 *
 * @param obj
 * @param props
 */
function copy(obj, props) {
    for (var k in props) {
        obj[k] = props[k];
    }
}

/**
 *
 * @param Fn
 * @param props
 */
function copyStatics(Fn, props) {
    if (!isObject(props))return;
    eachKeys(props, function (key, value, scope) {
        Fn[key] = isFunction(value) ? function () {
            return value.apply(scope, arguments);
        } : value;
    });
}

/**
 *
 * @param old
 * @param src
 */
function override(old, src) {
    if (isObject(src)) {
        copy(old, src);
    }
}

/**
 * A deep clone utility stolen from jquery. forgive me!
 */
function clone(source, destanation) {
    var options, name, src, copy, copyIsArray, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;

    // Handle a deep copy situation
    if (typeof target === "boolean") {
        deep = target;

        // skip the boolean and the target
        target = arguments[ i ] || {};
        i++;
    }

    // Handle case when target is a string or something (possible in deep copy)
    if (typeof target !== "object" && !isFunction(target)) {
        target = {};
    }

    // extend itself if only one argument is passed
    if (i === length) {
        target = this;
        i--;
    }

    for (; i < length; i++) {
        // Only deal with non-null/undefined values
        if ((options = arguments[ i ]) != null) {
            // Extend the base object
            for (name in options) {
                src = target[ name ];
                copy = options[ name ];

                // Prevent never-ending loop
                if (target === copy) {
                    continue;
                }

                // Recursion process if we're merging plain objects or arrays
                if (deep && copy && ( isPlainObject(copy) || (copyIsArray = isArray(copy)) )) {
                    if (copyIsArray) {
                        copyIsArray = false;
                        clone = src && isArray(src) ? src : [];

                    } else {
                        clone = src && isPlainObject(src) ? src : {};
                    }

                    // Never move original objects, clone them
                    target[name] = clone(deep, clone, copy);

                    // Don't bring in undefined values
                } else if (copy !== undefined) {
                    target[name] = copy;
                }
            }
        }
    }

    // Return the modified object
    return target;
}
/**
 * deep clone helper
 * @param target
 * @returns {*}
 */
function deepClone(target) {
    switch (type(target)) {
        case '[object Array]':
            return clone(true, target, []);

        case '[object Boolean]':
            return Boolean(target);

        case '[object Date]':
            return new Date(+target);

        case '[object Object]':
            return clone(true, target, {});

        case '[object Number]':
        case '[object String]':
            return new target.constructor(target);

        case '[object RegExp]':
            return new target.constructor(target.source,
                (target.global ? 'g' : '') +
                    (target.ignoreCase ? 'i' : '') +
                    (target.multiline ? 'm' : ''));
    }
}

/**
 *
 * @param name
 * @param object
 * @param scope
 * @returns {*|w}
 */
function exportObj(name, object, scope) {
    var parts = name.split('.'), i = 0, len = parts.length, ck = len - 1, root = arguments[2] || window;
    eachArray(parts, function (part, i) {
        if (i === ck) {
            root[part] = object;
        } else {
            root[part] = root[part] || {};
        }
        root = root[part];
    });
    s.modules[name] = root;
    return root;
}

/**
 *
 * @param path
 * @param k
 * @param v
 */
function setScope(path, k, v) {
    var scope = s.modules.scope[path] = {};
    scope[k] = v;
}

/**
 *
 * @param path
 * @returns {*}
 */
function getScope(path) {
    return s.modules.scope[path];
}

/**
 *
 * @param argArray
 * @returns {Object}
 */
Function.prototype.alias = function (argArray) {
    var inst = Object.create(this.prototype);
    this.apply(inst, argArray);
    return inst;
};