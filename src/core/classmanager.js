"use strict";
/**
 *
 * @type {RegExp}
 */
var classNameReg = /^(([\w\*][\w_\d\*]*(\.[\w\*][\w_\d\*]*)*)\.)?([\w\*][\w_\d\*]*)/;

/**
 *
 * @type {Object}
 */
var methodChainedCache = {};

/**
 *
 * @param to
 * @returns {*}
 */
var setId = function (to) {
    var id = s.modules.genId();
    to.getId = function () {
        return id;
    };
    return id;
};

/**
 *
 * @param mixin
 * @param clazz
 * @param is_class
 */
function mixin(mixin, clazz, is_class) {
    //copy mixin function's prototype
    eachArray(mixin, function (mix) {
        if (is_class) {
            var object = s.modules[mix];
            if (object && object.prototype) {
                copy(clazz.constructor.prototype, object.prototype);
            }
        } else {
            copy(clazz.prototype, mix);
        }
    });
}
/**
 *
 * @param factory
 * @param name
 * @param rep
 * @returns {*|Function}
 */
var extractExtend = function (factory, name, rep) {
    var temp = rep || Loop,
        obj = factory[name];
    if (!isUndefined(obj)) {
        temp = s.modules[obj];
        delete factory[name];
        return temp;
    }
    return temp;
};

/**
 *
 * @param factory
 * @param name
 * @returns {*}
 */
var extractObject = function (factory, name) {
    var temp = factory[name] || 0;
    if (temp) {
        delete factory[name];
        return temp;
    }
    return 1;
};

/**
 *
 * @param supplier
 * @param callback
 * @param receiver
 * @returns {*}
 */
var bindExceptFn = function (supplier, callback, receiver) {
    for (var k in supplier) {
        if (supplier.hasOwnProperty(k)) {
            var v = supplier[k];
            if (isFunction(v)) {
                callback(k, v, receiver);
            } else {
                receiver[k] = v;
            }
        }
    }
    return receiver;
};

/**
 *
 * @param name
 * @param arg
 * @param thisArg
 * @returns {*}
 */
var applySingleton = function (name, arg, thisArg) {
    if (s.modules.scope._[name]) {
        if (arg.callee._singletonInstance) {
            return arg.callee._singletonInstance;
        }
        arg.callee._singletonInstance = thisArg;
    }
};

/**
 *
 * @param obj
 * @param name
 * @param args
 * @returns {*}
 */
function callSuper(obj, name, args) {
    var isConstructor = isArguments(arguments[1]);
    var validMethod = noGapInString(name) && !!obj[name];
    var scope = getScope(obj.absoluteName);
    var Class = s.modules[obj.absoluteName];

    if (!scope) {
        throw new Error("Cannot call callSuper on a class without extends parent class!");
    }

    if (isConstructor) {
        var superClass = scope["origins"][obj.className];
        if (superClass && isFunction(superClass)) {
            superClass.apply(obj, arguments[1]);
            return true;
        }
    } else if (validMethod) {
        var findMemberChained = function (clazz, obj) {
            var member = clazz.members[name];
            if (!member) {
                return findMemberChained(clazz.parentClass, name);
            }
            methodChainedCache[obj.getId()] = clazz.parentClass;
            return member;
        };
        var member = findMemberChained(methodChainedCache[obj.getId()] || Class, obj);
        return member.apply(obj, args);
    }
}
/**
 *
 * @param absoluteName
 * @param members
 * @return {Function}
 */
function defineClass(absoluteName, members) {
    /*extract class name from absolute name*/
    var className = classNameReg.exec(absoluteName)[4];
    /*extract class constructor from members*/
    var constructor = isFunction(members[className]) ? members[className] : function () {
    };
    /*delete the fake constructor*/
    delete members[className];

    //noinspection FunctionWithInconsistentReturnsJS
    var constructorMethod = function () {
        var applier = applySingleton(absoluteName, arguments, this);
        if (applier) {
            return applier;
        }
        setId(this);
        constructor.apply(this, arguments);
    };
    //extract extend class
    var Parent = 1,
        obj = members['extend'],
        Initializer;
    if (!isUndefined(obj)) {
        Parent = s.modules[obj];
        delete members['extend'];
        if (isFunction(Parent)) {
            var origins = {};
            origins[className] = Parent;
            //mark origins
            setScope(absoluteName, "origins", origins);
            Initializer = function () {
                //public members
                setId(this);
                this.absoluteName = absoluteName;
                this.className = className;
                //public methods bound to the class
                bindExceptFn(members, function (key, value, root) {
                    var original = root[key];
                    if (original) {
                        origins[key] = original;
                        root[key] = value;
                    } else {
                        root[key] = value;
                    }
                }, this);

                constructor.apply(this, arguments);
            };
            Initializer.prototype = new Parent();
            Initializer.members = members;
            Initializer.parentClass = Parent;
            Initializer.absoluteName = absoluteName;
            Initializer.className = className;
            Initializer.constructor = constructor;
            return Initializer;
        }
    }

    constructorMethod.absoluteName = absoluteName;
    constructorMethod.className = className;
    constructorMethod.prototype = members;
    constructorMethod.members = members;
    constructorMethod.constructor = constructor;
    return constructorMethod;
}

/**
 *
 * @param absoluteName
 * @param factory
 */
s.define = function (absoluteName, factory) {
    var clazz;
    var typo = isFunction(arguments[0]) ? 0 : (isArray(arguments[1]) ? 1 : 2);
    switch (typo) {
        case 0://s.define(FUNCTION)
            var name = "s.anonymous.Class_" + s.modules.genId();
            clazz = exportObj(name, arguments[0]);
            break;
        case 1://s.define(NAME,ARRAY,FUNCTION|FACTORY)
            var Clazz;
            if (isFunction(arguments[2])) {//s.define(NAME,ARRAY,FUNCTION)
                //note: in this way, all method expose out must ref to context 'this'
                Clazz = arguments[2];
                mixin(arguments[1], arguments[2], false);
            } else {//s.define(NAME,ARRAY,FACTORY)
                s.define(arguments[0], arguments[2]);
                Clazz = s.modules[arguments[0]];
                mixin(arguments[1], Clazz, false);
            }
            exportObj(absoluteName, Clazz.alias(arguments[3]));
            break;
        case 2://s.define(NAME,FACTORY)
            clazz = defineClass(absoluteName, factory);
            //add statics support
            var statics = extractObject(factory, 'statics');
            if (statics !== 1) {
                var stack = [];
                //statics is the one shared in all class instances,
                //the changes of static item are open
                var staticsKeyEscape = "parentClass,className,constructor";
                var iterate = function (clazz) {
                    var values = {};
                    var host = clazz.parentClass;
                    if (host) {
                        eachKeysExcept(host, staticsKeyEscape, function (k, v) {
                            values[k] = deepClone(v);
                        });
                        //add to stack
                        stack.push(values);
                        //iterate parent
                        iterate(host)
                    }
                };
                iterate(clazz);
                stack.reverse();
                stack.push(statics);
                eachArray(stack, function (node) {
                    copyStatics(clazz, node);
                });

                delete factory["statics"];
            }
            //add mixin support
            var m = extractObject(factory, 'mixin');
            if (m !== 1) {
                mixin(m, clazz, true);
            }
            /**
             *
             */
            exportObj(absoluteName, clazz);
            break;
    }
};