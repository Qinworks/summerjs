/**
 * An object holds the app runtime properties
 * s.Runtime which read/write properties on the s.modules.runtime
 */
s.define("s.Runtime", [/*NO_PROTOTYPE_MIXIN*/], {
    /**
     * all properties store here
     */
    properties: {},
    /**
     *
     * @param key
     * @param value
     */
    setProperty: function (key, value) {
        this.properties[key] = value;
    },
    /**
     *
     * @param key
     * @returns {*}
     */
    getProperty: function (key) {
        return this.properties[key];
    },
    /**
     *
     */
    normalize: function () {
        this.properties = {};
    }
});