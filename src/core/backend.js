/**
 *
 */
s.Loader.js("src/libs/jquery.js", function () {
    /**
     *
     * @param selector
     * @param context
     * @returns {*}
     */
    s.modules.selector = function (selector, context) {
        return jQuery(selector, context);
    };
    /**
     *
     */
    copy(s, jQuery);
    /**
     *
     * @type {boolean}
     */
    s.Runtime.setProperty("backend", "jquery");
    /**
     * just use jquery dom ready function
     * @param callback
     */
    s(document).ready(function () {
        eachArray(s.props.ready, function (callback) {
            callback();
        });
    });
});