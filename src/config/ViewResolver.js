s.define("s.config.ViewResolver", {
    /**
     *
     * @param element
     * @constructor
     */
    ViewResolver: function (element) {
        this.element = element;
        this.mappings = {};
        this.loaded = {};
    },
    /**
     *
     * @param label
     * @returns {*}
     */
    isLoaded: function (label) {
        return  this.loaded[label];
    },
    /**
     *
     * @param label
     * @param url
     */
    addPath: function (label, url) {
        if (s(this.element).find(label).length > 0) {
            this.mappings[label] = url;
        }
    },
    /**
     *
     * @param label
     */
    loadPath: function (label) {
        //cancel when node not contained in the element or asset loaded already
        if (!this.mappings[label] || this.loaded[label]) {
            return;
        }
        //do load asset
        var aThis = this;
        var path = this.mappings[label];
        if (path) {
            s(label).load(path, function (content, response) {
                aThis.loaded[label] = (response === "success");
            });
        }
    },
    /**
     *
     * @param label
     */
    forceLoad: function (label) {
        this.loaded[label] = false;
        this.loadPath(label);
    }
});