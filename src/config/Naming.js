s.define("s.config.Naming",[],{
    /**
     * --------------------------------------
     * -------- Scroll Class Naming ---------
      */
    SCROLL_WRAPPER:".view-wrapper",
    SCROLL_CONTENT:".view-scroller",
    SCROLL_UNIT:".scroll-unit-wrapper",
    SCROLL_VIEW:".view"
});