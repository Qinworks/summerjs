/**
 *
 */
s.define("s.config.Template", {
    /**
     *
     */
    extend: "s.config.ViewResolver",
    /**
     *
     * @constructor
     */
    Template: function () {
        callSuper(this, arguments);
        this.content = "";
    },
    /**
     *
     * @returns {*}
     */
    getContainer: function () {
        return s(this.element);
    },
    /**
     *
     * @param container
     * @param val
     * @returns {*}
     */
    addFragment: function (container, val) {
        var fragment;
        var ele = this.getContainer().find(container);
        if (ele.length > 0) {
            fragment = s(val).appendTo(ele);
        }
        return fragment;
    },
    /**
     *
     * @param container
     * @param val
     * @returns {*}
     */
    addFragmentBefore: function (container, val) {
        var fragment;
        var ele = this.getContainer().find(container);
        if (ele.length > 0) {
            fragment = s(val).prependTo(ele);
        }
        return fragment;
    },
    /**
     *
     * @param ele
     * @returns {*}
     */
    getFragment: function (ele) {
        return  this.getContainer().find(ele);
    },
    /**
     *
     * @param content
     * @param prepend
     */
    setContent: function (content, prepend) {
        var pre = isBoolean(arguments[1]) ? prepend : true;
        this.content = content;
        if(pre){
            this.getContainer().prepend(s(content));
        }else{
            this.getContainer().append(s(content));
        }
    },
    /**
     *
     * @returns {string}
     */
    getContent: function () {
        return this.content;
    },
    /**
     *
     */
    statics: {
        /**
         *
         * @param ele
         * @returns {s.config.Template}
         */
        from: function (ele) {
            return new s.config.Template(ele);
        }
    }
});