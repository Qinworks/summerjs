s.define("s.ui.Component", {
    /**
     *
     * @constructor
     */
    Component: function () {
        this._delayCalls = [];
        this._componentReady = false;
        /*init properties holder*/
        s.Runtime.setProperty(this.className + "_" + this.getId(), {});
    },
    /**
     * dom element contains this component
     */
    container: null,
    /**
     *
     */
    initComponent: function (container) {
        /*if already called*/
        if (this._componentReady) {
            return;
        }
        /*set*/
        this.container = container;
        /**************************************************
         *
         * There should be a ui construction call implemented by sub class
         *
         * **/

        /*set ready*/
        this._componentReady = true;
        /*call delay functions*/
        var scope = this;
        eachArray(this._delayCalls, function (callback) {
            callback.apply(scope, null);
        });
    },
    /**
     *
     * @param action
     * @param callback
     */
    performUIAction: function (action, callback) {
        if (this.isComponentReady()) {
            callback.apply(this, null);
        } else {
            this._delayCalls.push(callback);
        }
    },

    /**
     *
     * @returns {boolean|*}
     */
    isComponentReady: function () {
        return this._componentReady;
    },
    /**
     * get the scroll view properties
     * @returns {*}
     */
    getProperties: function () {
        return s.Runtime.getProperty(this.className + "_" + this.getId());
    },
    /**
     *
     * @param key
     * @param value
     */
    setProperty: function (key, value) {
        if (noGapInString(key)) {
            this.getProperties()[key] = value;
        }
    }
});