s.define("s.ui.NavigationBar", {
    /**
     *
     */
    extend: "s.ui.Component",
    /**
     * some basic templates to build a navigation bar
     */
    statics: {
        /*normal button on the bar right*/
        BAR_BUTTON_OPTIONS_NORMAL: "normal",
        /*active state button on the bar right*/
        BAR_BUTTON_OPTIONS_ACTIVE: "active",
        /*warn state button on the bar right*/
        BAR_BUTTON_OPTIONS_WARNING: "warning",
        /**
         * navigation bar div wrapper
         */
        NAV_BAR_TEMPLATE: '<div class="navigation-bar default" id="navigation-bar">' +
            '<div class="navigation-bar-item">' +
            '   <div class="bar-items-wrapper">' +
            '       <div class="bar-title">Untitled</div>' +
            '   </div>' +
            '</div>' +
            '</div>',
        /**
         * get button template by type
         * @param title
         * @param type
         * @returns {string}
         */
        getButtonTemplate: function (title, type) {
            console.log(title, type);
            var style = "";
            if (noGapInString(arguments[2])) {
                style = arguments[2] + "-state"
            }
            return '<div class="button bar-button button-ios-6 ' + type + ' ' + style + '"><div class="button-label">' + (title) + '</div></div>';
        }
    },
    /**
     *
     * @constructor
     */
    NavigationBar: function () {
        callSuper(this, arguments);
    },
    /**
     *
     * @param container
     */
    initComponent: function (container) {
        this.template = s.config.Template.from(container);
        this.template.setContent(s.ui.NavigationBar.NAV_BAR_TEMPLATE);

        callSuper(this, "initComponent", arguments);
    },
    /**
     *
     * @param title
     */
    setTitle: function (title) {
        this.performUIAction("setTitle", function () {
            this.template.getFragment(".bar-title").text(title);
        });
    },
    /**
     *
     * @param title
     * @param onBackClicked
     * @param type
     * @param style
     */
    addBarButton: function (title, onBackClicked, type, style) {
        if (/back|forward|options/.test(type)) {
            var typeVal = type.indexOf("back") != -1 ? 1 : (type.indexOf("forward") != -1 ? 2 : 3);
            var buttonStyle = (style === "active" ? "active" : (style === "warning" ? "warning" : "normal"));
            var buttonType, buttonTemplate;
            this.performUIAction("addBarButton", function () {
                switch (typeVal) {
                    case 1:
                        /*if already an back button added before, hide that*/
                        if (this.getBackButton().length > 0) {
                            this.getBackButton().hide();
                        }
                        buttonType = "button-back";
                        buttonTemplate = s.ui.NavigationBar.getButtonTemplate(title, buttonType);
                        this.template.addFragmentBefore(".bar-items-wrapper", buttonTemplate);
                        break;
                    case 2:
                        /*if already an forward button added before, abort this action*/
                        if (this.getForwardButton().length > 0) {
                            return;
                        }
                        /*hide the option button*/
                        if (this.getOptionButton().length > 0) {
                            this.getOptionButton().hide();
                        }
                        buttonType = "button-forward";
                        buttonTemplate = s.ui.NavigationBar.getButtonTemplate(title, buttonType);
                        this.template.addFragment(".bar-items-wrapper", buttonTemplate);
                        break;
                    case 3:
                        /*if already an option button added before, abort this action*/
                        if (this.getOptionButton().length > 0) {
                            return;
                        }
                        /*hide the forward button*/
                        if (this.getForwardButton().length > 0) {
                            this.getForwardButton().hide();
                        }
                        buttonType = "button-options";
                        buttonTemplate = s.ui.NavigationBar.getButtonTemplate(title, buttonType, buttonStyle);
                        this.template.addFragment(".bar-items-wrapper", buttonTemplate);
                        break;
                }

                this.getTemplate().
                    getFragment(".button-ios-6." + buttonType).
                    mousedown(function () {
                        s(this).addClass("selected");
                    }).on('mouseup mouseout', function () {
                        s(this).removeClass("selected");
                    });
            });
        }
    },
    /**
     *
     * @param title
     * @param onBackClicked
     */
    addBackButton: function (title, onBackClicked) {
        if (isString(title)) {
            var callback = isFunction(arguments[1]) ? onBackClicked : function () {/*EMPTY*/
            };
            this.addBarButton(title, callback, "back");
        }
    },
    /**
     *
     * @returns {*}
     */
    getBackButton: function () {
        return this.isComponentReady() ? this.template.getFragment(".bar-button.button-back") : null;
    },
    /**
     *
     * @param title
     * @param onBackClicked
     * @param style
     */
    addForwardButton: function (title, onBackClicked, style) {
        if (isString(title)) {
            var callback = isFunction(arguments[1]) ? onBackClicked : function () {/*EMPTY*/
            };
            this.addBarButton(title, callback, "forward", style);
        }
    },
    /**
     *
     * @returns {*}
     */
    getForwardButton: function () {
        return this.isComponentReady() ? this.template.getFragment(".bar-button.button-forward") : null;
    },
    /**
     *
     * @param title
     * @param onBackClicked
     * @param style
     */
    addOptionButton: function (title, onBackClicked, style) {
        if (isString(title)) {
            console.log(style);
            var callback = isFunction(arguments[1]) ? onBackClicked : function () {/*EMPTY*/
            };
            this.addBarButton(title, callback, "options", style);
        }
    },
    /**
     *
     * @returns {*}
     */
    getOptionButton: function () {
        return this.isComponentReady() ? this.template.getFragment(".bar-button.button-options") : null;
    },
    /**
     *
     * @param button
     */
    setGroupButton: function (button) {
        this.performUIAction("setGroupButton", function () {
            //todo add code here
        });
    },
    /**
     *
     * @returns {*}
     */
    getTemplate: function () {
        return this.template;
    },
    /**
     *
     * @returns {boolean}
     */
    isComponentReady: function () {
        var result = callSuper(this, "isComponentReady", arguments);
        return result && !!this.template;
    }
})
;