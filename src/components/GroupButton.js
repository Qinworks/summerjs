s.define("s.ui.GroupButton", {
    /**
     *
     * @constructor
     */
    GroupButton: function () {
        this.size = 0;
        this.template = s.config.Template(document.body);
        this.template.setContent(
            '<div data-role="button-group" class="button-group button-group-layout-horizontal"></div>');
        this.buttonTemplate =
            '    <div data-role="button" class="button">' +
                '       <div data-role="label" class="text button-label">Untitled + ' + this.size + '</div>' +
                '</div>';
    },
    addButton: function () {

    },
    /**
     *
     * @returns {*}
     */
    getTemplate: function () {
        return this.template;
    }
});