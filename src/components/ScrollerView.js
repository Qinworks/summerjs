s.define("s.ui.ScrollView", {
    /**
     *
     */
    extend: "s.ui.Component",
    /**
     *
     */
    statics: {
        /**
         *
         */
        SCROLL_TEMPLATE: "<div class='view'><div class='view-wrapper'><div class='view-scroller'></div></div></div>",
        /**
         *
         */
        SCROLL_UNIT_TEMPLATE: "<ul class='scroll-unit-wrapper'></ul>",
        /**
         *
         */
        GROUPED_SCROLL_UNIT_TEMPLATE: "<ul class='grouped-scroll-unit-wrapper'></ul>",
        /**
         *
         * @returns {s.ui.ScrollView}
         */
        fromTemplate: function () {
            return new s.ui.ScrollView(s.config.Naming.SCROLL_WRAPPER);
        }
    },
    /**
     *
     * @param container
     * @param grouped
     * @constructor
     */
    ScrollView: function (container, grouped) {
        /**
         *
         */
        callSuper(this, arguments);
        /**
         * check element valid
         * @type {*}
         */
        var ele = document.querySelector(container);
        if (ele.length) {
            throw new Error("invalid arguments to build a ScrollView:" + container);
        }
        /**
         * build scroll with simple basic options
         * @type {s.ui.Scroll}
         */
        var view = new s.ui.Scroll(ele, {
            mouseWheel: true,
            scrollbars: true,
            interactiveScrollbars: true,
            shrinkScrollbars: 'scale',
            fadeScrollbars: true
        });
        /**
         * return as private/cannot modified
         * @returns {s.ui.Scroll}
         */
        this.getView = function () {
            return view;
        };
        /**
         *
         * @type {s.config.Template}
         */
        var template = s.config.Template.from(ele);
        /**
         * return as private/cannot modified
         * @returns {s.config.Template}
         */
        this.getTemplate = function () {
            return template;
        };
        /**
         * init component at last
         */
        this.initComponent(container);
        /**
         *
         * @type {Boolean}
         */
        this.grouped = isBoolean(arguments[1]) ? grouped : true;
    },
    /**
     *
     * @type {Boolean}
     */
    grouped: true,
    /**
     *
     */
    addUnit: function (unitName) {

        /*instance a new view unit*/
        var unit = new s.ui.ScrollViewUnit(".view-scroller", unitName, this.grouped);

        /*store properties bind to this unit*/
        var properties = {
            container: unit,
            children: {}
        };
        this.setProperty("SCROLL_UNIT_" + unitName, properties);

        /*update the scroll view*/
        var ref = this;
        setTimeout(function () {
            ref.refresh();
        }, 1);

        /*return the unit we built*/
        return unit;
    },
    /**
     *
     */
    refresh: function () {
        if (this.isComponentReady()) {
            //refresh the scroll container
            this.getView().refresh();
        }
    },
    /**
     *
     */
    showScrollbars: function () {
        this.performUIAction("showScrollbars", function () {
            var opt = this.getView().options;
            opt.scrollbars = true;
            opt.interactiveScrollbars = true;
            opt.shrinkScrollbars = 'scale';
            opt.fadeScrollbars = true;

            this.getView().refresh();
        });
    },
    /**
     *
     */
    hideScrollbars: function () {
        this.performUIAction("hideScrollbars", function () {
            var opt = this.getView().options;
            opt.scrollbars = false;
            opt.interactiveScrollbars = false;
            opt.shrinkScrollbars = undefined;
            opt.fadeScrollbars = false;

            this.getView().refresh();
        });
    },
    /**
     *
     * @returns {boolean|*}
     */
    isComponentReady: function () {
        return this.getView() instanceof s.ui.Scroll
            && callSuper(this, "isComponentReady", arguments);
    }
});