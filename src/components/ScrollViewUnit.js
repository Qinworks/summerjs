s.define("s.ui.ScrollViewUnit", {
    extend: "s.ui.Component",
    statics: {
        /**
         *
         * @param title
         * @param id
         * @returns {string}
         */
        newHeader: function (title, id) {
            return ('<span class="list-item grouped-list-header" id="unit_title_' + id + '">' + title + '</span>');
        },
        /**
         *
         * @param id
         * @param grouped
         * @returns {string}
         */
        newUnit: function (id, grouped) {
            var d = (isNumber(id) || noGapInString(arguments[0])) ? id : null;
            var g = isBoolean(arguments[1]) ? grouped : true;
            if (!d) {
                throw "invalid id to generate a unit:" + id;
            }
            d = "unit_wrapper_" + d;
            return (g ? "<ul class='grouped-scroll-unit-wrapper' id='" + d + "'></ul>" : "<ul class='scroll-unit-wrapper' id='" + d + "'></ul>");
        },
        /**
         *
         * @param title
         * @param grouped
         * @param groupedButtonType
         * @param imageKey
         * @returns {string}
         */
        newUnitEntry: function (title, grouped, groupedButtonType, imageKey) {
            var g = isBoolean(arguments[1]) ? grouped : true;
            var t = isNumber(arguments[2]) ? groupedButtonType : 0;
            var i = isString(arguments[3]) ? imageKey : null;
            return "<li class='list-item " + (g ? "grouped-list-style" : "plain-list-style") + "'><span class='list-item-img'></span> " + title + "<span class='list-item-button'></span></li>";
        }
    },
    /**
     * @type {s.config.Template}
     */
    template: null,
    /**
     *  @type {Boolean}
     */
    grouped: true,
    /**
     *
     * @constructor
     */
    ScrollViewUnit: function (container, title, grouped) {
        if (!s(container).length) {
            throw new Error("invalid arguments to start a new ScrollViewUnit!");
        }
        callSuper(this, arguments);
        this.grouped = grouped;
        this.initComponent(container, title);
    },
    /**
     *
     * @param container
     * @param title
     */
    initComponent: function (container, title) {
        callSuper(this, "initComponent", arguments);
        /*init template*/
        this.template = s.config.Template.from(container);
        /*append skeleton to the dom*/
        this.template.setContent(s.ui.ScrollViewUnit.newHeader(title, this.getId()) +
            s.ui.ScrollViewUnit.newUnit(this.getId(), this.grouped), /*not prepend to the container*/false);
    },
    /**
     *
     * @param title
     * @param groupedButton
     * @param imageKey
     * @returns {*}
     */
    addEntry: function (title, groupedButton, imageKey) {
        var entry = s.ui.ScrollViewUnit.newUnitEntry(title,this.grouped, groupedButton, imageKey);
        var wrapper = this.template.getFragment("#unit_wrapper_" + this.getId());
        var itemInst = s(entry).appendTo(wrapper);

        itemInst.mousedown(function () {
            s(this).addClass("selected");
        }).on('mouseup mouseout', function () {
                s(this).removeClass("selected");
            });
        //store the item
        var unit = this.getProperties();
        unit.children = unit.children || {};
        unit.children[title] = itemInst;

        return itemInst;
    },
    /**
     *
     */
    setTitle: function (text) {
        if (isString(text)) {
            this.template.getFragment("#unit_title_" + this.getId()).text(text);
        }
    }
});