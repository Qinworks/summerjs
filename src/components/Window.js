s.define("s.ui.Window", {
    /**
     *
     * @constructor
     */
    Window: function () {
        /*initialize a template from body node*/
        this.template = s.config.Template.from("#summer");

        /*start build a window*/
        /*append window skeleton div to body*/
        this.template.setContent("<div class='window'>" + s.ui.ScrollView.SCROLL_TEMPLATE + "</div>");
        /*build a scrollable view in this window*/
        var view = s.ui.ScrollView.fromTemplate();
        /**
         * override getView
         * @returns {s.ui.ScrollView}
         */
        this.getView = function () {
            return view;
        };
    },
    /**
     * must call initComponent to return the initialized scroll view
     */
    getView: function () {
        return null;
    },
    /**
     *
     * @param bar
     */
    setNavigationBar: function (bar) {
        if (bar instanceof s.ui.NavigationBar) {
            bar.initComponent(".window");
        }
    }
});