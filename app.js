/**
 *
 */
var main;
s.onReady(function () {
    var bar = new s.ui.NavigationBar();
    bar.setTitle("Hello, Summer!");
    bar.addBackButton("Home");
    bar.addOptionButton("Settings", null, s.ui.NavigationBar.BAR_BUTTON_OPTIONS_WARNING);
    main = new s.ui.Window();
    main.setNavigationBar(bar);
    var view = main.getView();
    var app = view.addUnit("Applications");
    for (var i = 0; i < 6; i++) {
        app.addEntry("Element " + i, null, null);
    }
    var circular = view.addUnit("Circular");
    for (i = 0; i < 6; i++) {
        circular.addEntry("Element " + i, null, null);
    }
});